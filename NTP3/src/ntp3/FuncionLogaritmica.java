/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ntp3;

import java.util.ArrayList;

/**
 *
 * @author cristobal
 */
public class FuncionLogaritmica implements Funcion{
    int nIncog=2;
    double rangosSup[]={12.1,5.8};
    double rangosInf[]={-3,4.1};
    
    public FuncionLogaritmica() {
        
    }
    
    @Override
    public Double evaluar(double sol[]) {
        //3x^2 + 2x + 5
        double x=sol[0];
        double y=sol[1];
        return 21.5+x*Math.sin(4*Math.PI*x)+y*Math.sin(20*Math.PI*y);
    }

    @Override
    public int getNumIncog() {
        return nIncog;
    }
    //Devuelve los rangos superiores del problema
    @Override
    public double[] getSuperior() {
       return rangosSup;
    }
    //Devuelve los rangos inferiores del problema
    @Override
    public double[] getInferior() {
        return rangosInf;
    }
    
}
