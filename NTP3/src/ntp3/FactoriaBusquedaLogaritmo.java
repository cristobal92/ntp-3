/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ntp3;

import java.util.ArrayList;

/**
 *
 * @author Rubens
 */
public class FactoriaBusquedaLogaritmo extends FactoriaBusqueda{

    public EstadoBusqueda estado;
    public ArrayList<BusquedaSolucion> buscadores;
    public int nBuscadores=4;
    
    @Override
    public void generarBusqueda() {
        estado=EstadoBusqueda.getInstance();
        Funcion f=new FuncionLogaritmica();
        Estrategia est1=new EstrategiaAleatorio();
        Estrategia est2=new AscencionColinas();
        estado.setFuncion(f);
        buscadores=new ArrayList<>();
        for (int i = 0; i < nBuscadores/2; i++) {
            buscadores.add(new BusquedaSolucion(est2, i));
        }
        for (int i = nBuscadores/2; i < nBuscadores; i++) {
            buscadores.add(new BusquedaSolucion(est1, i));
        }
        buscadores.stream().forEach((buscador) -> {
            buscador.start();
        });
    }
    
}
