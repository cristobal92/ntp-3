/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ntp3;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Rubens
 */
public class BusquedaSolucion extends Thread{
    EstadoBusqueda estadoBusqueda;
    Estrategia estrategiaActual;
    Funcion funcion;
    double[] solGlobal;
    double[] solLocal;
    boolean continua;
    int idBuscador, nPasos=500;
    
    public BusquedaSolucion(Estrategia e, int id)
    {
      estadoBusqueda = EstadoBusqueda.getInstance();
      estadoBusqueda.registrarBuscador(this);
      funcion=estadoBusqueda.getFuncion();
      //est.addBuscador(this);
      solGlobal = estadoBusqueda.getSolucion();
      solLocal=new double[funcion.getNumIncog()];
      estrategiaActual=e;
      continua=true;
      idBuscador=id;
    }
    
    public synchronized void actualizarMejorSolucion(double[] solucion){
        solGlobal=solucion;
        //solLocal=solucion;
        solLocal=Arrays.copyOf(solGlobal, solGlobal.length);
    }
    
    public void setEstrategia(Estrategia e)
    {
       estrategiaActual=e;
    }
    private void mensaje(String m){
        System.out.println(this.idBuscador+"->"+m);
    }
    @Override
    public void run(){
        int contador=0;
        while(contador<nPasos){
            mensaje("Solucion local="+Arrays.toString(solLocal)+" Global="+Arrays.toString(solGlobal));
            solLocal=estrategiaActual.siguienteSolucion(funcion, solLocal);
            double valorLocal=funcion.evaluar(solLocal);
            double valorGlobal=funcion.evaluar(solGlobal);
            if(valorGlobal<valorLocal){
                mensaje("Actualizando solucion global "+Arrays.toString(solLocal));
                estadoBusqueda.setSolucion(solLocal);
                contador=0;
            }
            contador++;
        }
        estadoBusqueda.eliminarBuscador(this);
    }
}
