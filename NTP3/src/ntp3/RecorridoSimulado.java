/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ntp3;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Rubens
 */
public class RecorridoSimulado extends Estrategia {

    @Override
    public double[] siguienteSolucion(Funcion f, double sol[]) {
        int control = 10;
        int recontrol=0;
        int local = 2;
        double[] inf = f.getInferior();
        double[] sup = f.getSuperior();
       
        //Mutacion        
        for(int a = 0; a < inf.length; a++)
        {
            //Path temporal = new Path();
            Random r = new Random();
            double cutPoint1 = inf[a] + (sup[a] - inf[a]) * r.nextDouble();
            if(cutPoint1 == 0) cutPoint1++;	
            double cutPoint2 = cutPoint1;
            while(cutPoint2 == cutPoint1 || cutPoint2 < 1)
                cutPoint2 = inf[a] + (sup[a] - inf[a]) * r.nextDouble();



            if(sol[a]-cutPoint2 > cutPoint1-cutPoint2 && cutPoint1-cutPoint2 > 0)
            {
                double[] aux = null;
                aux[0] = cutPoint1-cutPoint2;
                double eval = f.evaluar(aux);
                if(sol[a]<eval)
                    sol[a]=eval;

            }
        }
        

        return sol;
    }
}
