/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ntp3;

import java.util.ArrayList;

/**
 *
 * @author Rubens
 */
public class FactoriaBusquedaPolinomial extends FactoriaBusqueda{

    public EstadoBusqueda estado;
    public ArrayList<BusquedaSolucion> buscadores;
    public int nBuscadores=4;
    
    @Override
    public void generarBusqueda() {
        estado=EstadoBusqueda.getInstance();
        Funcion f=new FuncionPolinomial();
        Estrategia est1=new EstrategiaAleatorio();
        estado.setFuncion(f);
        buscadores=new ArrayList<>();
        for (int i = 0; i < nBuscadores; i++) {
            buscadores.add(new BusquedaSolucion(est1, i));
        }
        buscadores.stream().forEach((buscador) -> {
            buscador.start();
        });
    }
    
}
