/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ntp3;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Rubens
 */
public class AscencionColinas extends Estrategia{
    double escala=0.1;
    double[] solucionAnterior;
    int dir=1;
    @Override
    public double[] siguienteSolucion(Funcion f,double sol[]) {
      Random r = new Random();
      double[] inf = f.getInferior();
      double[] sup = f.getSuperior();
      double[] salida=new double[sol.length];
      
      for(int i=0; i<inf.length;i++)
      {
          salida[i] = sol[i]+dir*escala;
          salida[i]=(salida[i]>sup[i])?sup[i]:salida[i];
          salida[i]=(salida[i]<inf[i])?inf[i]:salida[i];
      }
      if(f.evaluar(sol)>f.evaluar(salida)){
          dir=-1;
          for(int i=0; i<inf.length;i++)
      {
          salida[i] = sol[i]+dir*escala;
          salida[i]=(salida[i]>sup[i])?sup[i]:salida[i];
          salida[i]=(salida[i]<inf[i])?inf[i]:salida[i];
      }
      }
      solucionAnterior=sol;
      return salida;
     
    }
    
}
