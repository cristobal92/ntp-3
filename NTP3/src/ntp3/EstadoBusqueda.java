/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ntp3;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Rubens
 */
public class EstadoBusqueda {
    
    private double[] mejorSolucion=null;
    public Funcion problema;
    private static EstadoBusqueda INSTANCE = null;
    private ArrayList<BusquedaSolucion> buscadores; 
    //ArrayList<BusquedaSolucion> buscadores;    
    private EstadoBusqueda(){   
        buscadores=new ArrayList<>();
    }

    public static EstadoBusqueda getInstance() {
        if (INSTANCE == null) INSTANCE = new EstadoBusqueda();
        return INSTANCE;
    }
    public synchronized void registrarBuscador(BusquedaSolucion busc){
        buscadores.add(busc);
    }
    public synchronized void eliminarBuscador(BusquedaSolucion busc){
        buscadores.remove(busc);
        //Si todos se han eliminado se imprime la solucion final
        if(buscadores.isEmpty()){
            System.out.println("\033[32mSolucion encontrada ="+Arrays.toString(mejorSolucion)+" devuelve un valor de "+problema.evaluar(mejorSolucion)+"\033[0m");
        }
    }
    //Funcion para actualizar el valor de la solucion global
    private void notificar(){
        double[] sol=this.getSolucion();
        
        buscadores.stream().forEach((busc) -> {
            busc.actualizarMejorSolucion(Arrays.copyOf(sol,sol.length));
        });
    }
    /*
    Obtener una copia de la solucion, pueden usarlo varias hebras por lo que es inseguro pasar una referencia del original
    */
    public double[] getSolucion()
    {
        return Arrays.copyOf(mejorSolucion,mejorSolucion.length);
    }   
    public synchronized void setSolucion(double[] sol)
    {
        //Verificamos que sea mejor
        if(problema.evaluar(sol)>problema.evaluar(mejorSolucion)){
            mejorSolucion=Arrays.copyOf(sol,sol.length);
            //Notificamos al resto de buscadores(observadores)
            this.notificar();
        }
       
    }
    /*
    Metodo para establecer la funcion de la que debe encontrarse la mejor solucion
    */
    public void setFuncion(Funcion f)
    {
        problema=f;
        mejorSolucion=new double[f.getNumIncog()];
    }
    
    public Funcion getFuncion()
    {
        return problema;
    }

}
