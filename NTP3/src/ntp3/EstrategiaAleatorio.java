/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ntp3;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Rubens
 */
public class EstrategiaAleatorio extends Estrategia{

    @Override
    public double[] siguienteSolucion(Funcion f,double sol[]) {
      Random r = new Random();
      double[] inf = f.getInferior();
      double[] sup = f.getSuperior();
      for(int i=0; i<inf.length;i++)
      {
          sol[i] = inf[i] + (sup[i] - inf[i]) * r.nextDouble();
      }
      return sol;
     
    }
    
}
