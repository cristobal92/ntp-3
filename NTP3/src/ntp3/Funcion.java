/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ntp3;

import java.util.ArrayList;

/**
 *
 * @author Rubens
 */
public interface Funcion {  
    public Double evaluar(double sol[]);
    public int getNumIncog();
    public double[] getSuperior();
    public double[] getInferior();
}
