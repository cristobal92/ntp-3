/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ntp3;

import java.util.ArrayList;

/**
 *
 * @author cristobal
 */
public class FuncionPolinomial implements Funcion{
    int nIncog=1;
    double rangosSup[]={2};
    double rangosInf[]={-2};
    
    public FuncionPolinomial() {
        
    }
    
    @Override
    public Double evaluar(double sol[]) {
        //3x^2 + 2x + 5
        double x=sol[0];
        return 3*x*x+2*x+5;
    }

    @Override
    public int getNumIncog() {
        return nIncog;
    }
    //Devuelve los rangos superiores del problema
    @Override
    public double[] getSuperior() {
       return rangosSup;
    }
    //Devuelve los rangos inferiores del problema
    @Override
    public double[] getInferior() {
        return rangosInf;
    }
    
}
